<?php

namespace Drupal\qna\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the comment entity.
 *
 * @see \Drupal\qna\Entity\QnaCommentEntity.
 */
class QnaCommentEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\qna\Entity\QnaCommentEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished qna comment entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published qna comment entities');

      case 'update':
        if ($account->hasPermission('edit any qna comment entities')) {
          return AccessResult::allowed();
        }

        return AccessResult::allowedIf($entity->getOwnerId() == $account->id() && $account->hasPermission('edit own qna comment entities'));

      case 'delete':
        if ($account->hasPermission('delete any qna comment entities')) {
          return AccessResult::allowed();
        }

        return AccessResult::allowedIf($entity->getOwnerId() == $account->id() && $account->hasPermission('delete own qna comment entities'));
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add qna comment entities');
  }

}
