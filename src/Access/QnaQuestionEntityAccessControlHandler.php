<?php

namespace Drupal\qna\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Blank entity.
 *
 * @see \Drupal\qna\Entity\QnaQuestionEntity.
 */
class QnaQuestionEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\qna\Entity\QnaQuestionEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished qna question entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published qna question entities');

      case 'update':
        if ($account->hasPermission('edit any qna question entities')) {
          return AccessResult::allowed();
        }

        return AccessResult::allowedIf($entity->getOwnerId() == $account->id() && $account->hasPermission('edit own qna question entities'));

      case 'delete':
        if ($account->hasPermission('delete any qna question entities')) {
          return AccessResult::allowed();
        }

        return AccessResult::allowedIf($entity->getOwnerId() == $account->id() && $account->hasPermission('delete own qna question entities'));
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add qna question entities');
  }

}
