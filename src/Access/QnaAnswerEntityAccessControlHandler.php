<?php

namespace Drupal\qna\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\qna\QnaManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for the Answer entity.
 *
 * @see \Drupal\qna\Entity\QnaAnswerEntity.
 */
class QnaAnswerEntityAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * QnaManager.
   *
   * @var \Drupal\qna\QnaManager
   */
  protected $qnaManager;

  /**
   * CurrentRouteMatch.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * QnaAnswerEntityAccessControlHandler constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   EntityTypeInterface.
   * @param \Drupal\qna\QnaManager $qna_manager
   *   QnaManager.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   CurrentRouteMatch.
   */
  public function __construct(EntityTypeInterface $entity_type, QnaManager $qna_manager, CurrentRouteMatch $route_match) {
    parent::__construct($entity_type);

    $this->qnaManager = $qna_manager;
    $this->currentRouteMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('qna.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\qna\Entity\QnaAnswerEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished qna answer entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published qna answer entities');

      case 'update':
        if ($account->hasPermission('edit any qna answer entities')) {
          return AccessResult::allowed();
        }

        return AccessResult::allowedIf($entity->getOwnerId() == $account->id() && $account->hasPermission('edit own qna answer entities'));

      case 'delete':
        if ($account->hasPermission('delete any qna answer entities')) {
          return AccessResult::allowed();
        }

        return AccessResult::allowedIf($entity->getOwnerId() == $account->id() && $account->hasPermission('delete own qna answer entities'));
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $question = $this->currentRouteMatch->getParameters()->get('qna_question');

    // If current request is AJAX get Question entity from another parameter.
    if (!$question) {
      $question = $this->currentRouteMatch->getParameters()->get('qna_entity')->get('qna_question')->entity;
    }

    $is_answered = $this->qnaManager->isUserAlreadyAddAnswer($account, $question);

    return AccessResult::allowedIf(!$is_answered)
      ->andIf(AccessResult::allowedIfHasPermission($account, 'add qna answer entities'));
  }

}
