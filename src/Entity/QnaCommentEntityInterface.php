<?php

namespace Drupal\qna\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining comment entities.
 *
 * @ingroup qna
 */
interface QnaCommentEntityInterface extends QnaEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Sets a related entity to which the comment refers.
   *
   * @param \Drupal\qna\Entity\QnaEntityInterface $entity
   *   The Question or Answer entity.
   *
   * @return \Drupal\qna\Entity\QnaCommentEntityInterface
   *   The called comment entity.
   */
  public function setRelatedEntity(QnaEntityInterface $entity);

  /**
   * Get a parent entity, which was set by setRelatedEntity method.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The Question or Answer entity.
   */
  public function getRelatedEntity();

  /**
   * Get a Question entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The Question entity.
   */
  public function getQuestion();

}
