<?php

namespace Drupal\qna\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * QnaEntityInterface.
 */
interface QnaEntityInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the question creation timestamp.
   *
   * @return int
   *   Creation timestamp of the question.
   */
  public function getCreatedTime();

  /**
   * Sets the question creation timestamp.
   *
   * @param int $timestamp
   *   The question creation timestamp.
   *
   * @return \Drupal\qna\Entity\QnaQuestionEntityInterface
   *   The called question entity.
   */
  public function setCreatedTime($timestamp);

}
