<?php

namespace Drupal\qna\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\qna\Plugin\Field\QnaAnswersFieldItemList;
use Drupal\user\UserInterface;

/**
 * Defines the Blank entity.
 *
 * @ingroup qna
 *
 * @ContentEntityType(
 *   id = "qna_question",
 *   label = @Translation("Question"),
 *   handlers = {
 *     "view_builder" = "Drupal\qna\Entity\QnaQuestionViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\qna\Form\QnaQuestionEntityForm",
 *       "add" = "Drupal\qna\Form\QnaQuestionEntityForm",
 *       "edit" = "Drupal\qna\Form\QnaQuestionEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\qna\Routing\QnaQuestionHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\qna\Access\QnaQuestionEntityAccessControlHandler",
 *   },
 *   base_table = "qna_question",
 *   data_table = "qna_question_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer qna question entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/qna_question/{qna_question}",
 *     "add-form" = "/qna_question/add",
 *     "edit-form" = "/qna_question/{qna_question}/edit",
 *     "delete-form" = "/qna_question/{qna_question}/delete",
 *     "collection" = "/admin/content/qna_question",
 *   },
 *   field_ui_base_route = "qna_question.settings"
 * )
 */
class QnaQuestionEntity extends ContentEntityBase implements QnaQuestionEntityInterface, EntityPublishedInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * Answers.
   *
   * @var \Drupal\qna\Entity\QnaAnswerEntityInterface[]
   */
  protected $answers;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    $comments = $this->get('comments')->referencedEntities();
    $answers = $this->getAnswers();

    // Remove referenced comment entities.
    foreach ($comments as $comment) {
      $comment->delete();
    }

    // Remove referenced answer entities.
    foreach ($answers as $answer) {
      $answer->delete();
    }

    parent::delete();
  }

  /**
   * {@inheritdoc}
   */
  public function getAnswers() {
    if (!$this->answers) {
      $storage = $this->entityTypeManager()->getStorage('qna_answer');
      $answer_ids = $storage->getQuery()
        ->condition('qna_question', $this->id())
        ->execute();

      if ($answer_ids) {
        $this->answers = $storage->loadMultiple($answer_ids);
      }
    }

    return $this->answers;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['qna_question'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Question'))
      ->setDescription(t('Text of question.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 0,
        'settings' => [
          'rows' => 12,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'weight' => 0,
        'label' => 'above',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Blank is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The username of the content author.'))
      ->setSetting('handler', 'default')
      ->setSetting('target_type', 'user')
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['comments'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Comments'))
      ->setDescription(t('Comments of this entity.'))
      ->setSetting('handler', 'default')
      ->setSetting('target_type', 'qna_comment')
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'qna_comments',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['answers'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Answers'))
      ->setName('answers')
      ->setDescription(t('This are the answers to the question.'))
      ->setComputed(TRUE)
      ->setClass(QnaAnswersFieldItemList::class)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'qna_answers',
        'weight' => 15,
      ]);

    return $fields;
  }

}
