<?php

namespace Drupal\qna\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Theme\Registry;
use Drupal\Core\Url;
use Drupal\qna\QnaManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * QnaAnswerViewBuilder.
 */
class QnaAnswerViewBuilder extends EntityViewBuilder {

  /**
   * The qnaManager.
   *
   * @var \Drupal\qna\QnaManager
   */
  protected $qnaManager;

  /**
   * QnaAnswerViewBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   EntityTypeInterface.
   * @param \Drupal\qna\QnaManager $qna_manager
   *   qnaManager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   EntityRepositoryInterface.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   LanguageManagerInterface.
   * @param \Drupal\Core\Theme\Registry|null $theme_registry
   *   Registry.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface|null $entity_display_repository
   *   EntityDisplayRepositoryInterface.
   */
  public function __construct(EntityTypeInterface $entity_type, QnaManager $qna_manager, EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager, Registry $theme_registry = NULL, EntityDisplayRepositoryInterface $entity_display_repository = NULL) {
    parent::__construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository);

    $this->qnaManager = $qna_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('qna.manager'),
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('theme.registry'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    parent::buildComponents($build, $entities, $displays, $view_mode);

    /** @var \Drupal\qna\Entity\QnaCommentEntityInterface[] $entities */
    if (empty($entities)) {
      return;
    }

    foreach ($entities as $id => $entity) {
      $bundle = $entity->bundle();
      $display = $displays[$bundle];
      // @TODO refactor this if will be problems with performance.
      $parent_question = $entity->getQuestion();
      $destination = Url::fromRoute('entity.qna_question.canonical',
        ['qna_question' => $parent_question->id()])->toString();

      if ($display->getComponent('links')) {
        $build[$id]['links'] = [
          '#prefix' => '<ul class="links inline">',
          '#suffix' => '</ul>',
        ];

        $build[$id]['links']['edit'] = [
          '#type' => 'link',
          '#title' => $this->t('Edit'),
          '#attributes' => [
            'class' => 'use-ajax',
          ],
          '#url' => Url::fromRoute('qna.action.edit_content', [
            'entity_type' => $entity->getEntityTypeId(),
            'qna_entity' => $entity->id(),
          ]),
          '#access' => $entity->access('update'),
          '#prefix' => '<li class="so-answer-edit">',
          '#suffix' => '</li>',
        ];

        $build[$id]['links']['delete'] = [
          '#type' => 'link',
          '#title' => $this->t('Delete'),
          '#attributes' => [
            'class' => 'use-ajax',
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode([
              'width' => 700,
            ]),
          ],
          '#url' => Url::fromRoute('entity.qna_answer.delete_form',
            ['qna_answer' => $entity->id()],
            ['query' => ['destination' => $destination]]
          ),
          '#access' => $entity->access('delete'),
          '#prefix' => '<li class="so-answer-delete">',
          '#suffix' => '</li>',
          '#attached' => ['library' => ['core/drupal.dialog.ajax']],
        ];
      }
    }
  }

}
