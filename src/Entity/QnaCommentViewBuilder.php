<?php

namespace Drupal\qna\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Url;

/**
 * QnaCommentViewBuilder.
 */
class QnaCommentViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    parent::buildComponents($build, $entities, $displays, $view_mode);

    /** @var \Drupal\qna\Entity\QnaCommentEntityInterface[] $entities */
    if (empty($entities)) {
      return;
    }

    foreach ($entities as $id => $entity) {
      $bundle = $entity->bundle();
      $display = $displays[$bundle];
      // @TODO refactor this if will be problems with performance.
      $parent_question = $entity->getQuestion();
      $destination = Url::fromRoute('entity.qna_question.canonical',
        ['qna_question' => $parent_question->id()])->toString();

      if ($display->getComponent('links')) {
        $build[$id]['links'] = [
          '#prefix' => '<ul class="links inline">',
          '#suffix' => '</ul>',
        ];

        $build[$id]['links']['edit'] = [
          '#type' => 'link',
          '#title' => $this->t('Edit'),
          '#attributes' => [
            'class' => 'use-ajax',
          ],
          '#url' => Url::fromRoute('qna.action.edit_content', [
            'entity_type' => $entity->getEntityTypeId(),
            'qna_entity' => $entity->id(),
          ]),
          '#access' => $entity->access('update'),
          '#prefix' => '<li class="so-comment-edit">',
          '#suffix' => '</li>',
        ];

        $build[$id]['links']['delete'] = [
          '#type' => 'link',
          '#title' => $this->t('Delete'),
          '#attributes' => [
            'class' => 'use-ajax',
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode([
              'width' => 700,
            ]),
          ],
          '#url' => Url::fromRoute('entity.qna_comment.delete_form',
            ['qna_comment' => $entity->id()],
            ['query' => ['destination' => $destination]]
          ),
          '#access' => $entity->access('delete'),
          '#prefix' => '<li class="so-comment-delete">',
          '#suffix' => '</li>',
          '#attached' => ['library' => ['core/drupal.dialog.ajax']],
        ];
      }
    }
  }

}
