<?php

namespace Drupal\qna\Entity;

/**
 * Provides an interface for defining Question entities.
 *
 * @ingroup qna
 */
interface QnaQuestionEntityInterface extends QnaEntityInterface {

  /**
   * Return dependent answers from the current question.
   *
   * @return \Drupal\qna\Entity\QnaAnswerEntityInterface[]
   *   Answer entities.
   */
  public function getAnswers();

}
