<?php

namespace Drupal\qna\Entity;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Theme\Registry;
use Drupal\qna\QnaManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * QnaQuestionViewBuilder.
 */
class QnaQuestionViewBuilder extends EntityViewBuilder {

  /**
   * QnaManager.
   *
   * @var \Drupal\qna\QnaManager
   */
  protected $qnaManager;

  /**
   * QnaQuestionViewBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   EntityTypeInterface.
   * @param \Drupal\qna\QnaManager $qna_manager
   *   qnaManager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   EntityRepositoryInterface.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   LanguageManagerInterface.
   * @param \Drupal\Core\Theme\Registry|null $theme_registry
   *   Registry.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface|null $entity_display_repository
   *   EntityDisplayRepositoryInterface.
   */
  public function __construct(EntityTypeInterface $entity_type, QnaManager $qna_manager, EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager, Registry $theme_registry = NULL, EntityDisplayRepositoryInterface $entity_display_repository = NULL) {
    parent::__construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository);

    $this->qnaManager = $qna_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('qna.manager'),
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('theme.registry'),
      $container->get('entity_display.repository')
    );
  }

}
