<?php

namespace Drupal\qna\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the comment entity.
 *
 * @ingroup qna
 *
 * @ContentEntityType(
 *   id = "qna_comment",
 *   label = @Translation("Comment"),
 *   handlers = {
 *     "view_builder" = "Drupal\qna\Entity\QnaCommentViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\qna\Form\QnaCommentEntityForm",
 *       "add" = "Drupal\qna\Form\QnaCommentEntityForm",
 *       "edit" = "Drupal\qna\Form\QnaCommentEntityForm",
 *       "edit_ajax" = "Drupal\qna\Form\QnaCommentEntityAjaxForm",
 *       "delete" = "\Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\qna\Routing\QnaCommentHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\qna\Access\QnaCommentEntityAccessControlHandler",
 *   },
 *   base_table = "qna_comment",
 *   data_table = "qna_comment_field_data",
 *   revision_table = "qna_comment_revision",
 *   revision_data_table = "qna_comment_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer qna comment entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/qna_comment/add",
 *     "edit-form" = "/admin/content/qna_comment/{qna_comment}/edit",
 *     "delete-form" = "/admin/content/qna_comment/{qna_comment}/delete",
 *     "version-history" = "/admin/content/qna_comment/{qna_comment}/revisions",
 *     "revision_revert" = "/admin/content/qna_comment/{qna_comment}/revisions/{qna_comment_revision}/revert",
 *     "revision_delete" = "/admin/content/qna_comment/{qna_comment}/revisions/{qna_comment_revision}/delete",
 *     "translation_revert" = "/admin/content/qna_comment/{qna_comment}/revisions/{qna_comment_revision}/revert/{langcode}",
 *     "collection" = "/admin/content/qna_comment",
 *   },
 *   field_ui_base_route = "qna_comment.settings"
 * )
 */
class QnaCommentEntity extends EditorialContentEntityBase implements QnaCommentEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * ContentEntityInterface.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $relatedEntity;

  /**
   * {@inheritdoc}
   */
  public function setRelatedEntity(QnaEntityInterface $entity) {
    $this->relatedEntity = $entity;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelatedEntity() {
    if (!$this->relatedEntity) {
      $storage = $this->entityTypeManager()->getStorage('qna_answer');
      $related_entity_id = $storage->getQuery()
        ->condition('comments', $this->id())
        ->execute();

      if (!$related_entity_id) {
        $storage = $this->entityTypeManager()->getStorage('qna_question');
        $related_entity_id = $storage->getQuery()
          ->condition('comments', $this->id())
          ->execute();
      }

      if ($related_entity_id) {
        $this->relatedEntity = $storage->load($related_entity_id[key($related_entity_id)]);
      }
    }

    return $this->relatedEntity;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $related_entity = $this->getRelatedEntity();

    if ($related_entity->getEntityTypeId() != 'qna_question') {
      return $related_entity->getQuestion();
    }

    return $related_entity;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * Remove item with current comment from parent entity.
   *
   * @return \Drupal\qna\Entity\QnaCommentEntity
   *   QnaCommentEntity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function deleteFromParentEntityField() {
    $related_entity = $this->getRelatedEntity();

    if ($comments = $related_entity->get('comments')->getValue()) {
      foreach ($comments as $key => $value) {
        if ($value['target_id'] == $this->id()) {
          $related_entity->get('comments')->removeItem($key);
          break;
        }
      }
    }

    $related_entity->save();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the qna_comment owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    if (!$update && $related_entity = $this->relatedEntity) {
      $related_entity->get('comments')->appendItem($this->id());
      $related_entity->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);

    foreach ($entities as $entity) {
      $entity->deleteFromParentEntityField();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Comment'))
      ->setDescription(t('Text of comment.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 0,
        'settings' => [
          'rows' => 12,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'weight' => 0,
        'label' => 'above',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Blank is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the comment entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
