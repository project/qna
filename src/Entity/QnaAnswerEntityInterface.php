<?php

namespace Drupal\qna\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Answer entities.
 *
 * @ingroup qna
 */
interface QnaAnswerEntityInterface extends QnaEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Get a Question entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The Question entity.
   */
  public function getQuestion();

}
