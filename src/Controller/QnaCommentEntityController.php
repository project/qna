<?php

namespace Drupal\qna\Controller;

use Drupal\Core\Link;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\qna\Entity\QnaCommentEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class QnaCommentEntityController.
 *
 *  Returns responses for comment routes.
 */
class QnaCommentEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a comment revision.
   *
   * @param int $qna_comment_revision
   *   The comment revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($qna_comment_revision) {
    $qna_comment = $this->entityTypeManager()->getStorage('qna_comment')
      ->loadRevision($qna_comment_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('qna_comment');

    return $view_builder->view($qna_comment);
  }

  /**
   * Page title callback for a comment revision.
   *
   * @param int $qna_comment_revision
   *   The comment revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($qna_comment_revision) {
    $qna_comment = $this->entityTypeManager()->getStorage('qna_comment')
      ->loadRevision($qna_comment_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $qna_comment->label(),
      '%date' => $this->dateFormatter->format($qna_comment->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a comment.
   *
   * @param \Drupal\qna\Entity\QnaCommentEntityInterface $qna_comment
   *   A comment object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(QnaCommentEntityInterface $qna_comment) {
    $account = $this->currentUser();
    $qna_comment_storage = $this->entityTypeManager()->getStorage('qna_comment');

    $langcode = $qna_comment->language()->getId();
    $langname = $qna_comment->language()->getName();
    $languages = $qna_comment->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $qna_comment->label()]) : $this->t('Revisions for %title', ['%title' => $qna_comment->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all qna comment revisions") || $account->hasPermission('administer qna comment entities')));
    $delete_permission = (($account->hasPermission("delete all qna comment revisions") || $account->hasPermission('administer qna comment entities')));

    $rows = [];

    $vids = $qna_comment_storage->revisionIds($qna_comment);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\qna\QnaCommentEntityInterface $revision */
      $revision = $qna_comment_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $qna_comment->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.qna_comment.revision', [
            'qna_comment' => $qna_comment->id(),
            'qna_comment_revision' => $vid,
          ]));
        }
        else {
          $link = $qna_comment->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.qna_comment.translation_revert', [
                'qna_comment' => $qna_comment->id(),
                'qna_comment_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.qna_comment.revision_revert', [
                'qna_comment' => $qna_comment->id(),
                'qna_comment_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.qna_comment.revision_delete', [
                'qna_comment' => $qna_comment->id(),
                'qna_comment_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['qna_comment_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
