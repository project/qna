<?php

namespace Drupal\qna\Controller;

use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class QnaAnswerEntityController.
 *
 *  Returns responses for Answer routes.
 */
class QnaQuestionEntityController extends EntityViewController {

  /**
   * {@inheritDoc}
   */
  public function view(EntityInterface $qna_question, $view_mode = 'full') {
    return parent::view($qna_question, $view_mode);
  }

}
