<?php

namespace Drupal\qna\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\qna\Entity\QnaEntityInterface;
use Drupal\qna\Event\BehaviorEvent;
use Drupal\qna\Event\QnaEvents;
use Drupal\qna\QnaManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * QnaAjaxActionsController.
 */
class QnaAjaxActionsController extends ControllerBase {

  /**
   * AjaxResponse.
   *
   * @var \Drupal\Core\Ajax\AjaxResponse
   */
  protected $ajaxResponse;

  /**
   * The qnaManager.
   *
   * @var \Drupal\qna\QnaManager
   */
  protected $qnaManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * QnaAjaxActionsController constructor.
   *
   * @param \Drupal\qna\QnaManager $qna_manager
   *   The qnaManager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  public function __construct(QnaManager $qna_manager, EventDispatcherInterface $eventDispatcher) {
    $this->qnaManager = $qna_manager;
    $this->ajaxResponse = new AjaxResponse();
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('qna.manager'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Implementation Edit link.
   *
   * @param \Drupal\qna\Entity\QnaEntityInterface $qna_entity
   *   QnaEntityInterface.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function editAction(QnaEntityInterface $qna_entity) {
    $form = $this->entityFormBuilder()->getForm($qna_entity, 'edit_ajax');
    $event = new BehaviorEvent($qna_entity);
    $this->eventDispatcher->dispatch(QnaEvents::COMMANDS_BEFORE_BUILD_FORM, $event);
    foreach ($event->getCommands() as $command) {
      $this->ajaxResponse->addCommand($command);
    }
    return $this->ajaxResponse->addCommand(new AppendCommand('#' . $qna_entity->getEntityTypeId() . '-' . $qna_entity->id(), $form));
  }

}
