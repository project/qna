<?php

namespace Drupal\qna\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the question edit forms.
 *
 * @ingroup qna
 */
class QnaQuestionEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    if ($this->entity->isNew()) {
      $form['#access'] = $this->entity->access('create');
    }
    else {
      $form['#access'] = $this->entity->access('update');
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Question.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Question.', [
          '%label' => $entity->label(),
        ]));
    }

    $form_state->setRedirect('entity.qna_question.canonical', ['qna_question' => $entity->id()]);
  }

}
