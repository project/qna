<?php

namespace Drupal\qna\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\qna\Event\BehaviorEvent;
use Drupal\qna\Event\QnaEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for comment edit forms.
 *
 * @ingroup qna
 */
class QnaCommentEntityForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    $instance->eventDispatcher = $container->get('event_dispatcher');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    $form_id = parent::getFormId();

    if ($related_entity = $this->entity->getRelatedEntity()) {
      $form_id .= '__' . $related_entity->getEntityTypeId() . "_" . $related_entity->id();
    }

    return $form_id;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\qna\Entity\QnaCommentEntity $entity */
    $form = parent::buildForm($form, $form_state);

    if ($this->entity->isNew()) {
      $form['#access'] = $this->entity->access('create');

      return $form;
    }

    $form['new_revision'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create new revision'),
      '#default_value' => FALSE,
      '#weight' => 10,
    ];
    $form['#access'] = $this->entity->access('update');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->account->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label comment.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label comment.', [
          '%label' => $entity->label(),
        ]));
    }
  }

  /**
   * Display Comment's content after save.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public function ajaxEditCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $event = new BehaviorEvent($this->getEntity());
    $this->eventDispatcher->dispatch(QnaEvents::COMMANDS_AFTER_SUBMIT_FORM, $event);
    foreach ($event->getCommands() as $command) {
      $response->addCommand($command);
    }

    return $response;
  }

}
