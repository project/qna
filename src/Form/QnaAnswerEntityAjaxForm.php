<?php

namespace Drupal\qna\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implement "edit_ajax" form for Answer entity.
 *
 * @see \Drupal\qna\Entity\QnaAnswerEntity
 */
class QnaAnswerEntityAjaxForm extends QnaAnswerEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['revision_log_message']['#access'] = FALSE;

    if (!is_null($form_state->get('is_new'))) {
      return $form;
    }

    $form['actions']['submit']['#ajax'] = [
      'callback' => '::ajaxEditCallback',
      'event' => 'click',
    ];

    $form['actions']['delete']['#access'] = FALSE;
    $form['actions']['submit']['#value'] = $this->t('Update');

    $form['actions']['cancel'] = [
      '#type' => 'button',
      '#value' => $this->t('Cancel'),
      '#weight' => 100,
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * Returns view display entity for ajax response.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   AjaxResponse.
   */
  public static function ajaxCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $response->addCommand(new InvokeCommand('[id*="qna_answer-"] > div', 'removeClass', ['visually-hidden']));
    $response->addCommand(new RemoveCommand('[id*="qna-answer-edit-ajax-form-"]'));

    return $response;
  }

}
