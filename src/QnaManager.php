<?php

namespace Drupal\qna;

use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\qna\Entity\QnaQuestionEntityInterface;
use Drupal\qna\Entity\QnaEntityInterface;

/**
 * QnaManager.
 */
class QnaManager {

  /**
   * EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * EntityFormBuilderInterface.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * QnaManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
   *   EntityFormBuilderInterface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   EntityTypeManagerInterface.
   */
  public function __construct(EntityFormBuilderInterface $entity_form_builder, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityFormBuilder = $entity_form_builder;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get add comment form for Question or Answer entity.
   *
   * @param \Drupal\qna\Entity\QnaEntityInterface $related_entity
   *   QnaEntityInterface.
   *
   * @return array
   *   Form render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCommentForm(QnaEntityInterface $related_entity) {
    /** @var \Drupal\qna\Entity\QnaCommentEntityInterface $entity */
    $entity = $this->entityTypeManager->getStorage('qna_comment')->create();
    $entity->setRelatedEntity($related_entity);

    return $this->entityFormBuilder->getForm($entity, 'edit_ajax', ['is_new' => TRUE]);
  }

  /**
   * Get list comment entities for Question or Answer entity.
   *
   * @param \Drupal\qna\Entity\QnaEntityInterface $related_entity
   *   QnaEntityInterface.
   *
   * @return array
   *   Build render array.
   */
  public function getRenderComments(QnaEntityInterface $related_entity) {
    $comments = $related_entity->get('comments')->referencedEntities();
    $build = [];

    if ($comments && $comments[key($comments)]->access('view')) {
      $build_comments = $this->entityTypeManager
        ->getViewBuilder('qna_comment')
        ->viewMultiple($comments);

      return $build_comments;
    }

    return $build;
  }

  /**
   * Get count of comments by entity.
   *
   * @param \Drupal\qna\Entity\QnaEntityInterface $related_entity
   *   QnaEntityInterface.
   *
   * @return int
   *   Count.
   */
  public function getCountComments(QnaEntityInterface $related_entity) {
    return $related_entity->get('comments')->count();
  }

  /**
   * Get list render Answers entities for Question.
   *
   * @param \Drupal\qna\Entity\QnaQuestionEntityInterface $question
   *   QnaQuestionEntityInterface.
   *
   * @return array
   *   Build render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getRenderAnswers(QnaQuestionEntityInterface $question) {
    $storage = $this->entityTypeManager->getStorage('qna_answer');
    $build = [];

    $answers_ids = $storage->getQuery()
      ->condition('qna_question', $question->id())
      ->execute();
    $answers = $storage->loadMultiple($answers_ids);

    if ($answers && $answers[key($answers)]->access('view')) {
      $view_builder = $this->entityTypeManager->getViewBuilder('qna_answer');
      $build_answers = $view_builder->viewMultiple($answers);
      $build['list'] = $view_builder->buildMultiple($build_answers);
    }

    return $build;
  }

  /**
   * Add answer form for Question entity.
   *
   * @param \Drupal\qna\Entity\QnaQuestionEntityInterface $question
   *   QnaQuestionEntityInterface.
   *
   * @return array
   *   Form render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAnswerForm(QnaQuestionEntityInterface $question) {
    $answer = $this->entityTypeManager->getStorage('qna_answer')->create();
    $answer->set('qna_question', $question->id());

    $form = $this->entityFormBuilder->getForm($answer, 'edit_ajax', ['is_new' => TRUE]);
    $form['#action'] = Url::fromRoute('entity.qna_question.canonical', [
      'qna_question' => $question->id(),
    ])->toString();

    return $form;
  }

  /**
   * Сhecks did the user has answered this question.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   AccountInterface.
   * @param \Drupal\qna\Entity\QnaQuestionEntityInterface $question
   *   QnaQuestionEntityInterface.
   *
   * @return bool
   *   Is user already add answer.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isUserAlreadyAddAnswer(AccountInterface $account, QnaQuestionEntityInterface $question) {
    $answer_id = $this->entityTypeManager->getStorage('qna_answer')
      ->getQuery()
      ->condition('uid', $account->id())
      ->condition('qna_question', $question->id())
      ->execute();

    return ($answer_id);
  }

}
