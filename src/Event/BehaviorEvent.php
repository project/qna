<?php

namespace Drupal\qna\Event;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\qna\Entity\QnaEntityInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event for add ajax command after submit ajax form.
 */
class BehaviorEvent extends Event {

  /**
   * The current entity.
   *
   * @var \Drupal\qna\Entity\QnaEntityInterface
   */
  protected $entity;

  /**
   * The ajax commands list.
   *
   * @var \Drupal\Core\Ajax\CommandInterface[]
   */
  protected $commands;

  /**
   * AjaxUpdateAnswer constructor.
   *
   * @param \Drupal\qna\Entity\QnaEntityInterface $entity
   *   The current answer entity.
   */
  public function __construct(QnaEntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Returns answer entity.
   *
   * @return \Drupal\qna\Entity\QnaEntityInterface
   *   The current answer entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Add ajax command.
   *
   * @param \Drupal\Core\Ajax\CommandInterface $command
   *   The new ajax command.
   */
  public function addCommand(CommandInterface $command) {
    $this->commands[] = $command;
  }

  /**
   * Set commands list.
   *
   * @param \Drupal\Core\Ajax\CommandInterface[] $commands
   *   The new commands list.
   */
  public function setCommands(array $commands) {
    $this->commands = $commands;
  }

  /**
   * Returns ajax commands.
   *
   * @return \Drupal\Core\Ajax\CommandInterface[]
   *   The ajax commands list.
   */
  public function getCommands() {
    return $this->commands ?: [];
  }

}
