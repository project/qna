<?php

namespace Drupal\qna\Event;

/**
 * Contains all events thrown in the Qna module.
 */
final class QnaEvents {

  /**
   * Event ID for add ajax command after submit ajax form.
   *
   * @Event
   *
   * @see \Drupal\qna\Event\BehaviorEvent
   */
  const COMMANDS_AFTER_SUBMIT_FORM = 'qna.after_submit.form';

  /**
   * Event ID for add ajax command before build ajax form.
   *
   * @Event
   *
   * @see \Drupal\qna\Event\BehaviorEvent
   */
  const COMMANDS_BEFORE_BUILD_FORM = 'qna.before_build.form';

}
