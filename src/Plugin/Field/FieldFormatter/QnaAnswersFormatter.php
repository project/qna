<?php

namespace Drupal\qna\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'QnaAnswers' formatter.
 *
 * @FieldFormatter(
 *   id = "qna_answers",
 *   label = @Translation("Qna Answers"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class QnaAnswersFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * EntityFormBuilderInterface.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFormBuilder = $container->get('entity.form_builder');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $view_builder = $this->entityTypeManager->getViewBuilder('qna_answer');
    $build['answers'] = $view_builder->viewMultiple($items->referencedEntities());
    $tmp_answer = $this->entityTypeManager->getStorage('qna_answer')->create();
    $tmp_answer->set('qna_question', $items->getEntity()->id());
    $form = $this->entityFormBuilder->getForm($tmp_answer, 'edit_ajax', ['is_new' => TRUE]);
    $form['#action'] = Url::fromRoute('entity.qna_question.canonical', [
      'qna_question' => $items->getEntity()->id(),
    ])->toString();
    $build['form'] = $form;

    $element[] = $build;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getTargetEntityTypeId() == 'qna_question' && $field_definition->getName() == 'answers';
  }

}
