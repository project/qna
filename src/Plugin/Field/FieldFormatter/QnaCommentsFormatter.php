<?php

namespace Drupal\qna\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'QnaComments' formatter.
 *
 * @FieldFormatter(
 *   id = "qna_comments",
 *   label = @Translation("Qna Comments"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class QnaCommentsFormatter extends EntityReferenceFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The qna manager.
   *
   * @var \Drupal\qna\QnaManager
   */
  protected $qnaManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->qnaManager = $container->get('qna.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $build['comments'] = $this->qnaManager->getRenderComments($items->getEntity());
    $build['form'] = $this->qnaManager->getCommentForm($items->getEntity());
    $element[] = $build;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    if ($field_definition->getName() != 'comments') {
      return FALSE;
    }
    $entity_type_id = $field_definition->getTargetEntityTypeId();
    return $entity_type_id == 'qna_question' || $entity_type_id == 'qna_answer';
  }

}
