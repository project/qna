<?php

namespace Drupal\qna\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * A computed field that provides a field answers for question.
 */
class QnaAnswersFieldItemList extends FieldItemList implements EntityReferenceFieldItemListInterface {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $storage = \Drupal::entityTypeManager()->getStorage('qna_answer');
    $answers_ids = $storage->getQuery()
      ->condition('qna_question', $this->getEntity()->id())
      ->execute();
    if (empty($answers_ids)) {
      return;
    }
    $delta = 0;
    foreach ($answers_ids as $answers_id) {
      $this->list[$delta] = $this->createItem($delta, ['target_id' => $answers_id]);
      $delta++;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function referencedEntities() {
    if ($this->isEmpty()) {
      return [];
    }
    $target_entities = $ids = [];
    foreach ($this->list as $delta => $item) {
      if ($item->target_id !== NULL) {
        $ids[$delta] = $item->target_id;
      }
    }
    // Load and add the existing entities.
    if ($ids) {
      $entities = \Drupal::entityTypeManager()->getStorage('qna_answer')->loadMultiple($ids);
      foreach ($ids as $delta => $target_id) {
        if (isset($entities[$target_id])) {
          $target_entities[$delta] = $entities[$target_id];
        }
      }
      // Ensure the returned array is ordered by deltas.
      ksort($target_entities);
    }

    return $target_entities;
  }

}
