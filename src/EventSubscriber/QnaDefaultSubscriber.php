<?php

namespace Drupal\qna\EventSubscriber;

use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\qna\Entity\QnaAnswerEntityInterface;
use Drupal\qna\Entity\QnaCommentEntityInterface;
use Drupal\qna\Event\BehaviorEvent;
use Drupal\qna\Event\QnaEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Questions and Answers event subscriber.
 */
class QnaDefaultSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * QnaDefaultSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      QnaEvents::COMMANDS_AFTER_SUBMIT_FORM => ['afterSubmitCommands'],
      QnaEvents::COMMANDS_BEFORE_BUILD_FORM => ['buildCommands'],
    ];
  }

  /**
   * Add default ajax commands.
   *
   * @param \Drupal\qna\Event\BehaviorEvent $event
   *   The event object.
   */
  public function afterSubmitCommands(BehaviorEvent $event) {
    $entity = $event->getEntity();
    $selector = '#' . $entity->getEntityTypeId() . '-' . $entity->id();
    if ($entity instanceof QnaAnswerEntityInterface) {
      $event->addCommand(new ReplaceCommand(
          $selector . ' .field--name-qna-answer',
          $entity->get('qna_answer')->view($this->fieldOptions('qna_answer', 'qna_answer.qna_answer.default')))
      );
      $event->addCommand(new ReplaceCommand(
          $selector . ' .field--name-created',
          $entity->get('created')->view($this->fieldOptions('created', 'qna_answer.qna_answer.default')))
      );

      $event->addCommand(new InvokeCommand($selector . ' > div', 'removeClass', ['visually-hidden']));
      $event->addCommand(new RemoveCommand($selector . ' .qna-answer-form'));
      return;
    }
    if ($entity instanceof QnaCommentEntityInterface) {
      $event->addCommand(new ReplaceCommand(
          $selector . ' .field--name-text',
          $entity->get('text')->view($this->fieldOptions('text', 'qna_comment.qna_comment.default')))
      );
      $event->addCommand(new ReplaceCommand(
          $selector . ' .field--name-created',
          $entity->get('created')->view($this->fieldOptions('created', 'qna_comment.qna_comment.default')))
      );
      $event->addCommand(new InvokeCommand($selector . ' > div', 'removeClass', ['visually-hidden']));
      $event->addCommand(new RemoveCommand($selector . ' .qna-comment-form'));
      return;
    }
  }

  /**
   * Add default ajax command befor build ajax edit form.
   *
   * @param \Drupal\qna\Event\BehaviorEvent $event
   *   The event object.
   */
  public function buildCommands(BehaviorEvent $event) {
    $entity = $event->getEntity();
    $event->addCommand(new InvokeCommand('#' . $entity->getEntityTypeId() . '-' . $entity->id() . ' > div', 'addClass', ['visually-hidden']));
  }

  /**
   * Returns field display options.
   *
   * @param string $field_name
   *   The field name.
   * @param string $display_id
   *   The entity display id.
   *
   * @return mixed
   *   The display options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function fieldOptions($field_name, $display_id) {
    $entityViewDisplay = $this
      ->entityTypeManager
      ->getStorage('entity_view_display')
      ->load($display_id);
    if ($entityViewDisplay instanceof EntityDisplayInterface) {
      return $entityViewDisplay->getComponent($field_name);
    }

    return [];
  }

}
