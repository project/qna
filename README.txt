CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The module allows you create your own Stack Overflow site with questions,
answers and comments

 * For a full description of the module, visit the project page:
   https://drupal.org/project/qna

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/qna


REQUIREMENTS
------------

Coming soon...


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Coming soon...


MAINTAINERS
-----------

Current maintainers:
 * Andrei Ivnitskii - https://www.drupal.org/u/ivnish
 * Miroslav Lee     - https://www.drupal.org/u/miroslav-lee
